import csv
import pandas as pd

import itertools
import os

import numpy as np
import pandas as pd
import tensorflow as tf

from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.metrics import confusion_matrix

from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.preprocessing import text, sequence
from keras import utils

from keras.models import load_model


data = pd.read_csv('test_scrubbed.csv',usecols=[0,1])
data = data[pd.notnull(data['lyrics'])]


train_size = int(len(data) * .8)
train_lyrics = data['lyrics'][:train_size]
train_genre = data['genre'][:train_size]

test_lyrics = data['lyrics'][train_size:]
test_genre = data['genre'][train_size:]

max_words = 10000
tokenize = text.Tokenizer(num_words=max_words, char_level=False)
tokenize.fit_on_texts(train_lyrics) # only fit on train

x_train = tokenize.texts_to_matrix(train_lyrics)
x_test = tokenize.texts_to_matrix(test_lyrics)

encoder = LabelEncoder()
encoder.fit(train_genre)
y_train = encoder.transform(train_genre)
y_test = encoder.transform(test_genre)

num_classes = np.max(y_train) + 1
y_train = utils.to_categorical(y_train, num_classes)
y_test = utils.to_categorical(y_test, num_classes)



# Build the model
model = Sequential()
model.add(Dense(256, input_shape=(max_words,)))
model.add(Dense(256, input_shape=(max_words,)))
model.add(Activation('relu'))
model.add(Dropout(0.7))
model.add(Dense(num_classes))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
              
history = model.fit(x_train, y_train,
                    batch_size=32,
                    epochs=2,
                    verbose=1,
                    validation_split=0.2)

score = model.evaluate(x_test, y_test,
                       batch_size=32, verbose=1)
                       
print(model.summary())
print('Test accuracy:', score[1])
model.save(str(score[1])+".pkl")

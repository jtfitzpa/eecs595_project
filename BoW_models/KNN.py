import csv
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

def readData():

    genre = []
    lyric = []
    f = open("test_scrubbed.csv", encoding="utf8", errors='ignore')
    for x in f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            genre.append(row[0])
            lyric.append(row[1])
    f.close()
    return genre, lyric



#bow_vectorizer = CountVectorizer(max_features=100, stop_words='english')#
#bow_vectorizer = HashingVectorizer(n_features=100)

bow_vectorizer = TfidfVectorizer(stop_words = 'english', sublinear_tf=True)

genres, lyrics = readData()
newLyrics = []
for x in lyrics:
    newLyrics.append(x.split())
x_train, x_test, y_train, y_test = train_test_split(lyrics,genres,test_size=0.2)

bowVect = bow_vectorizer.fit(x_train)
bowTrain = bowVect.transform(x_train)
bowTest = bowVect.transform(x_test)

knn = KNeighborsClassifier(n_neighbors=100)
knn.fit(bowTrain, y_train)



y_pred = knn.predict(bowTest)
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))

print ("x_train: %s, x_test: %s, y_train: %s, y_test: %s"%(len(x_train),len(x_test),len(y_train),len(y_test))) 










'''
Count Vectorization
5 - 47.3
7 - 50.9
10 - 52.7
80-55.4
90 - 56
100 - 55.2
200 - 54
300 -  53.5
500 - 53'''



'''
Hashing Vectorization
5 - 39.3
7 - 39.8
10 - 40.1
80 - 46.9
90 - 48.2
100 -49.1
200 - 49.2
300 -  49.0
500 - 46.4'''


'''
TfidfVectorizer
5 - 52
7 - 54.3
10 - 56.6
80 - 61.1
90 - 60.9
100 -62
200 - 61
300 -  58.7
500 - 58.4'''
import csv
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.ensemble import RandomForestClassifier

def readData():

    genre = []
    lyric = []
    f = open("test_scrubbed.csv", encoding="utf8", errors='ignore')
    for x in f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            genre.append(row[0])
            lyric.append(row[1])
    f.close()
    return genre, lyric



#bow_vectorizer = CountVectorizer(max_features=100, stop_words='english')#
#bow_vectorizer = HashingVectorizer(n_features=100)

bow_vectorizer = TfidfVectorizer(stop_words = 'english', sublinear_tf=True)

genres, lyrics = readData()
newLyrics = []
for x in lyrics:
    newLyrics.append(x.split())
x_train, x_test, y_train, y_test = train_test_split(lyrics,genres,test_size=0.2)

bowVect = bow_vectorizer.fit(x_train)
bowTrain = bowVect.transform(x_train)
bowTest = bowVect.transform(x_test)

rf = RandomForestClassifier(n_estimators=100,max_features="sqrt")
rf.fit(bowTrain,y_train)
accuracy = rf.score(bowTest, y_test)

print("Accuracy: ",accuracy)

print ("x_train: %s, x_test: %s, y_train: %s, y_test: %s"%(len(x_train),len(x_test),len(y_train),len(y_test))) 



#60

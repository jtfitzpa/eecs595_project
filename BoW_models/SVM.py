import csv
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn import metrics
from sklearn.svm import SVC

def readData():

    genre = []
    lyric = []
    f = open("test_scrubbed.csv", encoding="utf8", errors='ignore')
    for x in f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            genre.append(row[0])
            lyric.append(row[1])
    f.close()
    return genre, lyric


bow_vectorizer = TfidfVectorizer(stop_words = 'english', sublinear_tf=True)


genres, lyrics = readData()
newLyrics = []
for x in lyrics:
    newLyrics.append(x.split())
x_train, x_test, y_train, y_test = train_test_split(lyrics,genres,test_size=0.2)

bowVect = bow_vectorizer.fit(x_train)
bowTrain = bowVect.transform(x_train)
bowTest = bowVect.transform(x_test)

mnb = SVC(kernel='linear')
mnb.fit(bowTrain,y_train)
accuracy = mnb.score(bowTest,y_test)
print("Accuracy: ", accuracy)

print ("x_train: %s, x_test: %s, y_train: %s, y_test: %s"%(len(x_train),len(x_test),len(y_train),len(y_test))) 



#65%

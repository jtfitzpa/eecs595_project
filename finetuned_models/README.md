README for finetuned models

Easiest way to run is to use included .ipynb files in Google Colab in the notebooks folder

To run locally, download .py files in the python_files folder and use following setup instructions:

1. Install requred packages:
	torch, pandas, numpy, sklearn, transformers, matplotlib

2. Download data:
	Data can be pulled from our git repo using the following commands in the command line:
		
		!curl -o test.csv https://bitbucket.org/jtfitzpa/eecs595_project/raw/f75198ebabe3db8ca7e05692b543136b42bfd08a/data/test_scrubbed.csv
		!curl -o train.csv https://bitbucket.org/jtfitzpa/eecs595_project/raw/324252a5e6c5beabb1bdf174c38b0899dcf31438/data/train.csv
		!curl -o validate.csv https://bitbucket.org/jtfitzpa/eecs595_project/raw/324252a5e6c5beabb1bdf174c38b0899dcf31438/data/validate.csv



